from flask import Flask, send_from_directory

app = Flask(__name__)


@app.route("/")
def hello():
    return """
        <h1>Hello from Anton Myroniuk (IP-31mn)</h1>
        <img src="/static/rick.jpeg">
    """


@app.route("/static/<path:path>")
def serve_static(path):
    return send_from_directory("static", path)
