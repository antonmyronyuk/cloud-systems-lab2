import os

import sqlalchemy as sa
from flask import Flask, render_template_string
from redis.sentinel import Sentinel

app = Flask(__name__)

db_master = sa.create_engine((
    f'postgresql://{os.getenv("POSTGRES_MASTER_USER")}:{os.getenv("POSTGRES_MASTER_PASSWORD")}@'
    f'{os.getenv("POSTGRES_MASTER_HOST")}:{os.getenv("POSTGRES_MASTER_PORT")}/'
    f'{os.getenv("POSTGRES_MASTER_DB")}'
))
db_slave = sa.create_engine((
    f'postgresql://{os.getenv("POSTGRES_SLAVE_USER")}:{os.getenv("POSTGRES_SLAVE_PASSWORD")}@'
    f'{os.getenv("POSTGRES_SLAVE_HOST")}:{os.getenv("POSTGRES_SLAVE_PORT")}/'
    f'{os.getenv("POSTGRES_SLAVE_DB")}'
))

sentinel = Sentinel([(os.getenv("REDIS_SENTINEL_HOST"), os.getenv("REDIS_SENTINEL_PORT"))])
redis_master = sentinel.master_for(os.getenv("REDIS_SENTINEL_SERVICE_NAME"))
redis_slave = sentinel.slave_for(os.getenv("REDIS_SENTINEL_SERVICE_NAME"))


@app.route("/")
def index():
    # write operation
    page_visited_count = redis_master.incrby("cars:visit:counter", 1)

    # readonly operation
    with db_slave.connect() as connection:
        cars = connection.execute(sa.text("SELECT id, name, year FROM cars")).fetchall()

    return render_template_string(
        """<h2>Page visited: {{ page_visited_count }}</h2>
        Cars:
        <ul>
        {% for id, name, year in cars %}
            <li>ID: {{ id }}, name: {{ name }}, year: {{ year }}</li>
        {% endfor %}
        </ul>""",
        page_visited_count=page_visited_count,
        cars=cars,
    )


@app.route("/hello")
def hello():
    # readonly operation
    page_visited_count = int(redis_slave.get("cars:visit:counter") or 0)
    return render_template_string(
        """<h1>Hello from Anton Myroniuk (IP-31mn)</h1>
        <h2>Main page visited: {{ page_visited_count }}</h2>
        <img src="/static/rick.jpeg">""",
        page_visited_count=page_visited_count,
    )


@app.route("/increment-cars-year")
def increment_cars_year():
    # write operation
    with db_master.connect() as connection:
        connection.execute(sa.text("UPDATE cars SET year = year + 1 WHERE true;"))
        connection.commit()

    return render_template_string("Cars years incremented!")


@app.route("/clean-page-visit-counter")
def clean_page_visit_counter():
    # write operation
    redis_master.delete("cars:visit:counter")

    return render_template_string("Page visit counter cleaned!")
